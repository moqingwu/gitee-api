using System;
using System.Net.Http;
using System.Threading.Tasks;
using GiteeApi.Dtos;
using GiteeApi.Models;
using GiteeApi.Services;
using Microsoft.AspNetCore.Mvc;
using RestSharp;

namespace GiteeApi.Controllers
{
    /// <summary>
    /// Gitee API 控制器
    /// </summary>
    [Route("/")]
    [ApiController]
    public class GiteeAPIController : ControllerBase
    {
        /// <summary>
        /// Services
        /// </summary>
        private readonly GiteeAPIServices _giteeApiServices;

        /// <summary>
        /// 构造器
        /// </summary>
        /// <param name="giteeApiServices"></param>
        public GiteeAPIController(GiteeAPIServices giteeApiServices)
        {
            _giteeApiServices = giteeApiServices;
        }

        /// <summary>
        /// Get 请求
        /// </summary>
        /// <param name="userid">用户Id</param>
        /// <returns></returns>
        [HttpGet("{userid}")]
        public async Task<IActionResult> Get(int userid)
        {
            // 获取 Path
            string path = Request.Query["path"];
            if (path is null)
            {
                return Ok(new RequestBack(0, "请求路径不能为空！"));
            }

            // 获取 Access Token
            var accessToken = await _giteeApiServices.GetAccessToken(userid);
            if (accessToken.code == 0)
            {
                return Ok(accessToken);
            }
            
            // 请求地址获取
            var baseUrl = $"https://gitee.com/{string.Format(path, accessToken.data)}";
            
            // 发送请求
            var getResult = await new RestClient(baseUrl).GetAsync<object>(new RestRequest());
            
            // 设置请求链接
            return Ok(getResult);
        }
        
        /// <summary>
        /// Post 请求
        /// </summary>
        /// <param name="giteeApiDto">请求路径</param>
        /// <param name="userid">用户编号</param>
        /// <returns>请求结果</returns>
        [HttpPost("{userid}")]
        public async Task<IActionResult> Post(GiteeAPIPostDto giteeApiDto, int userid)
        {
            // 获取 Access Token
            var accessToken = await _giteeApiServices.GetAccessToken(userid, giteeApiDto.Password);
            if (accessToken.code == 0)
            {
                return Ok(accessToken);
            }
            giteeApiDto.Data.Add("access_token", accessToken.data.ToString());
            
            // 请求地址获取
            var baseUrl = $"https://gitee.com/{giteeApiDto.Path}";
 
            // 请求体数据
            var restRequest = new RestRequest().AddJsonBody(giteeApiDto.Data);
            
            // 发送请求
            var postResult = await new RestClient(baseUrl).PostAsync<object>(restRequest);
            
            return Ok(postResult);
        }
        
        /// <summary>
        /// Patch 请求
        /// </summary>
        /// <param name="giteeApiDto">请求路径</param>
        /// <param name="userid">用户编号</param>
        /// <returns>请求结果</returns>
        [HttpPatch("{userid}")]
        public async Task<IActionResult> Patch(GiteeAPIPostDto giteeApiDto, int userid)
        {
            // 获取 Access Token
            var accessToken = await _giteeApiServices.GetAccessToken(userid, giteeApiDto.Password);
            if (accessToken.code == 0)
            {
                return Ok(accessToken);
            }
            giteeApiDto.Data.Add("access_token", accessToken.data.ToString());
            
            // 请求地址获取
            var baseUrl = $"https://gitee.com/{giteeApiDto.Path}";
 
            // 请求体数据
            var restRequest = new RestRequest().AddJsonBody(giteeApiDto.Data);
            
            // 发送请求
            var postResult = await new RestClient(baseUrl).PatchAsync<object>(restRequest);
            
            return Ok(postResult);
        }
        
        /// <summary>
        /// Delete 请求
        /// </summary>
        /// <param name="giteeApiDto">请求路径</param>
        /// <param name="userid">用户编号</param>
        /// <returns>请求结果</returns>
        [HttpDelete("{userid}")]
        public async Task<IActionResult> Delete(GiteeAPIPostDto giteeApiDto, int userid)
        {
            // 获取 Access Token
            var accessToken = await _giteeApiServices.GetAccessToken(userid, giteeApiDto.Password);
            if (accessToken.code == 0)
            {
                return Ok(accessToken);
            }
            giteeApiDto.Data.Add("access_token", accessToken.data.ToString());
            
            // 请求地址获取
            var baseUrl = $"https://gitee.com/{giteeApiDto.Path}";
 
            // 请求体数据
            var restRequest = new RestRequest().AddJsonBody(giteeApiDto.Data);
            
            // 发送请求
            var postResult = await new RestClient(baseUrl).DeleteAsync<object>(restRequest);
            
            return Ok(postResult);
        }
    }
}