﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using GiteeApi.Dtos;
using GiteeApi.Models;
using GiteeApi.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace GiteeApi.Controllers
{
    /// <summary>
    /// 私钥控制器
    /// </summary>
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class AccessTokenController : ControllerBase
    {
        /// <summary>
        /// Services
        /// </summary>
        private readonly AccessTokenServices _accessTokenServices;

        /// <summary>
        /// AutoMapper
        /// </summary>
        private readonly IMapper _mapper;

        /// <summary>
        /// 构造器
        /// </summary>
        /// <param name="accessTokenServices"></param>
        public AccessTokenController(AccessTokenServices accessTokenServices, IMapper mapper)
        {
            _accessTokenServices = accessTokenServices;
            _mapper = mapper;
        }

        /// <summary>
        /// 添加私钥
        /// </summary>
        /// <param name="accessTokenDto">私钥；别名</param>
        /// <returns>添加结果</returns>
        [HttpPost]
        [Authorize]
        public async Task<IActionResult> Add(AddAccessTokenDto addAccessTokenDto)
        {
            addAccessTokenDto.UserId = Convert.ToInt32(Response.HttpContext.User.Identity.Name);
            return Ok(await _accessTokenServices.AccessTokenAddAsync(addAccessTokenDto));
        }

        /// <summary>
        /// 修改私钥
        /// </summary>
        /// <param name="updateAccessTokenDto">私钥Id；私钥；别名</param>
        /// <returns>修改结果</returns>
        [HttpPatch]
        [Authorize]
        public async Task<IActionResult> Update(UpdateAccessTokenDto updateAccessTokenDto)
        {
            updateAccessTokenDto.UserId = Convert.ToInt32(Response.HttpContext.User.Identity.Name);
            return Ok(await _accessTokenServices.AccessTokenUpdateAsync(updateAccessTokenDto));
        }

        /// <summary>
        /// 获取私钥
        /// </summary>
        /// <returns>私钥数据</returns>
        [HttpGet]
        [Authorize]
        public async Task<IActionResult> Get()
        {
            // 获取数据
            var accessToken =
                await _accessTokenServices.AccessTokenGetAsync(
                    Convert.ToInt32(Response.HttpContext.User.Identity.Name));

            // 自动映射
            List<GetAccessTokenDto> mapperResult = new List<GetAccessTokenDto>();
            foreach (var item in accessToken)
            {
                mapperResult.Add(_mapper.Map<GetAccessTokenDto>(item));
            }

            // 返回数据
            return Ok(new RequestBack(1, "获取成功", mapperResult));
        }

        /// <summary>
        /// 删除私钥
        /// </summary>
        /// <param name="Id">私钥Id</param>
        /// <returns>删除结果</returns>
        [HttpDelete]
        [Authorize]
        public async Task<IActionResult> Delete(int Id)
        {
            return Ok(await _accessTokenServices.AccessTokenDeleteAsync(
                Convert.ToInt32(Response.HttpContext.User.Identity.Name), Id));
        }
    }
}