﻿using AutoMapper;
using GiteeApi.Dtos;
using GiteeApi.Models;

namespace GiteeApi.Profiles
{
    public class UserInfoProfile : Profile
    {
        public UserInfoProfile()
        {
            CreateMap<UserInfo, UserInfoDto>();
        }
    }
}