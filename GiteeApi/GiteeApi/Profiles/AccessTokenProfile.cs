﻿using AutoMapper;
using GiteeApi.Dtos;
using GiteeApi.Models;

namespace GiteeApi.Profiles
{
    public class AccessTokenProfile : Profile
    {
        public AccessTokenProfile()
        {
            CreateMap<AccessToken, GetAccessTokenDto>();
        }
    }
}