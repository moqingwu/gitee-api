using System.ComponentModel.DataAnnotations;

namespace GiteeApi.Dtos
{
    public class ChangePasswordDto
    {
        /// <summary>
        /// 密码
        /// </summary>
        [Required(ErrorMessage = "密码不能为空")]
        public string password { get; set; }
    }
}