﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace GiteeApi.Dtos
{
    /// <summary>
    /// 注册实体
    /// </summary>
    public class RegisterDto : IValidatableObject
    {
        /// <summary>
        /// 邮箱验证码
        /// </summary>
        [Required(ErrorMessage = "邮箱验证码不能为空")]
        public string MailCode { get; set; }

        /// <summary>
        /// 邮箱
        /// </summary>
        [Required(ErrorMessage = "邮箱不能为空")]
        [MaxLength(50, ErrorMessage = "邮箱最大限制50位！")]
        [EmailAddress(ErrorMessage = "请输入正确的邮箱格式")]
        public string Email { get; set; }

        /// <summary>
        /// 密码
        /// </summary>
        [Required(ErrorMessage = "密码不能为空")]
        public string Password { get; set; }

        /// <summary>
        /// 确认密码
        /// </summary>
        [Required(ErrorMessage = "确认密码")]
        public string ConfirmPassword { get; set; }

        /// <summary>
        /// 自定义验证
        /// </summary>
        /// <param name="validationContext"></param>
        /// <returns></returns>
        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            // 两次密码是否一致
            if (Password != ConfirmPassword)
            {
                yield return new ValidationResult("两次密码不一致！", new[] {"RegisterDto"});
            }
        }
    }
}