using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace GiteeApi.Dtos
{
    public class GiteeAPIGetDto
    {
        /// <summary>
        /// 请求地址
        /// </summary>
        [Required(ErrorMessage = "请求地址不能为空")]
        public string Path { get; set; }
    }
}