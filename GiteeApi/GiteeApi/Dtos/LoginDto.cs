﻿using System.ComponentModel.DataAnnotations;

namespace GiteeApi.Dtos
{
    public class LoginDto
    {
        /// <summary>
        /// 邮箱
        /// </summary>
        [Required(ErrorMessage = "邮箱不能为空")]
        [MaxLength(50, ErrorMessage = "邮箱最大限制50位！")]
        public string Email { get; set; }
        
        /// <summary>
        /// 密码
        /// </summary>
        [Required(ErrorMessage = "密码不能为空")]
        public string Password { get; set; }
    }
}