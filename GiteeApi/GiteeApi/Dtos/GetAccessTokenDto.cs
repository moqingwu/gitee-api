﻿namespace GiteeApi.Dtos
{
    public class GetAccessTokenDto
    {
        /// <summary>
        /// 私钥 Id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// 私钥
        /// </summary>
        public string AccessToken1 { get; set; }
        
        /// <summary>
        /// 别名
        /// </summary>
        public string Alias { get; set; }
    }
}