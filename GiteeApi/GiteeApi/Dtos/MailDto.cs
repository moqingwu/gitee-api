using System.ComponentModel.DataAnnotations;

namespace GiteeApi.Dtos
{
    public class MailDto
    {
        /// <summary>
        /// 邮箱
        /// </summary>
        [Required(ErrorMessage = "邮箱不能为空")]
        [EmailAddress(ErrorMessage = "请输入正确的邮箱格式")]
        public string mail { get; set; }
    }
}