﻿using System.ComponentModel.DataAnnotations;

namespace GiteeApi.Dtos
{
    public class UpdateAccessTokenDto
    {
        /// <summary>
        /// 私钥 Id
        /// </summary>
        [Required(ErrorMessage = "私钥Id不能为空")]
        public int Id { get; set; }
        
        /// <summary>
        /// 用户 Id
        /// </summary>
        public int UserId { get; set; }
        
        
        /// <summary>
        /// 私钥
        /// </summary>
        [Required(ErrorMessage = "私钥不能为空")]
        [MaxLength(255, ErrorMessage = "长度不能超过255")]
        public string AccessToken1 { get; set; }
        
        /// <summary>
        /// 别名
        /// </summary>
        [Required(ErrorMessage = "别名不能为空")]
        [MaxLength(255, ErrorMessage = "长度不能超过255")]
        public string Alias { get; set; }
    }
}