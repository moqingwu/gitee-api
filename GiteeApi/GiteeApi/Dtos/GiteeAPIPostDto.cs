using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace GiteeApi.Dtos
{
    public class GiteeAPIPostDto
    {
        /// <summary>
        /// 密码
        /// </summary>
        [Required(ErrorMessage = "密码不能为空")]
        public string Password { get; set; }
        
        /// <summary>
        /// 请求地址
        /// </summary>
        [Required(ErrorMessage = "请求地址不能为空")]
        public string Path { get; set; }

        /// <summary>
        /// 请求体
        /// </summary>
        [Required(ErrorMessage = "请求体不能为空")]
        public Dictionary<string, string> Data { get; set; }
    }
}