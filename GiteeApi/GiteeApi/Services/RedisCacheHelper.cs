﻿using System;
using Microsoft.Extensions.Configuration;
using StackExchange.Redis;

namespace GiteeApi.Services
{
    /// <summary>
    /// Redis 封装
    /// </summary>
    public class RedisCacheHelper
    {
        // 设置地址
        private static readonly string ConnectionWriteString = "127.0.0.1:6379";
        // 连接服务器
        private static readonly IConnectionMultiplexer ConnMultiplexer =
            ConnectionMultiplexer.Connect(ConnectionWriteString);
        // 数据库位置
        private readonly IDatabase _db = ConnMultiplexer.GetDatabase(0);

        /// <summary>
        /// 设置
        /// </summary>
        /// <param name="key">key</param>
        /// <param name="value">value</param>
        /// <param name="expiry">过期时间</param>
        /// <returns></returns>
        public bool Set(string key, string value, TimeSpan? expiry = null)
        {
            return _db.StringSet(key, value, expiry);
        }

        /// <summary>
        /// 获取
        /// </summary>
        /// <param name="key">key</param>
        /// <returns></returns>
        public string Get(string key)
        {
            return _db.StringGet(key);
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="key">key</param>
        /// <returns></returns>
        public bool Del(string key)
        {
            return _db.KeyDelete(key);
        }
    }
}