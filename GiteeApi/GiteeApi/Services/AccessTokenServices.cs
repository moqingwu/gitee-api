﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GiteeApi.Dtos;
using GiteeApi.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace GiteeApi.Services
{
    public class AccessTokenServices
    {
        private readonly giteeapiContext _giteeapiContext;

        public AccessTokenServices(giteeapiContext giteeapiContext)
        {
            _giteeapiContext = giteeapiContext;
        }

        /// <summary>
        /// 添加私钥
        /// </summary>
        /// <param name="accessTokenDto"></param>
        /// <returns></returns>
        public async Task<RequestBack> AccessTokenAddAsync(AddAccessTokenDto addAccessTokenDto)
        {
            // 是否超过一条私钥
            var count = await _giteeapiContext.AccessTokens.CountAsync(x => x.UserId == addAccessTokenDto.UserId && x.Delete == 0);
            if (count >= 1)
            {
                return new(0, "私钥最大上限1个！");
            }
            
            // 录入数据
            AccessToken accessToken = new AccessToken
            {
                UserId = addAccessTokenDto.UserId,
                AccessToken1 = addAccessTokenDto.AccessToken1,
                Alias = addAccessTokenDto.Alias,
                Time = DateTime.Now
            };
            
            // 保存
            _giteeapiContext.AccessTokens.Add(accessToken);
            if (await _giteeapiContext.SaveChangesAsync() > 0)
            {
                return new(1, "添加成功");
            }

            return new(0, "添加失败");
        }

        /// <summary>
        /// 修改私钥
        /// </summary>
        /// <param name="updateAccessTokenDto"></param>
        /// <returns></returns>
        public async Task<RequestBack> AccessTokenUpdateAsync(UpdateAccessTokenDto updateAccessTokenDto)
        {
            // 获取数据
            var accessToken = await _giteeapiContext.AccessTokens.FirstOrDefaultAsync(x =>
                updateAccessTokenDto.Id == x.Id && x.UserId == updateAccessTokenDto.UserId);

            // 数据是否存在
            if (accessToken is null)
            {
                return new(0, "未找到数据");
            }

            // 修改
            accessToken.AccessToken1 = updateAccessTokenDto.AccessToken1;
            accessToken.Alias = updateAccessTokenDto.AccessToken1;

            // 保存
            if (await _giteeapiContext.SaveChangesAsync() > 0)
            {
                return new(1, "修改成功");
            }

            return new(0, "修改失败");
        }

        /// <summary>
        /// 获取私钥
        /// </summary>
        /// <param name="userId">用户Id</param>
        /// <returns></returns>
        public async Task<List<AccessToken>> AccessTokenGetAsync(int userId)
        {
            return await _giteeapiContext.AccessTokens.Where(x => x.UserId == userId && x.Delete == 0).ToListAsync();
        }
        
        /// <summary>
        /// 删除私钥
        /// </summary>
        /// <param name="userId">用户Id</param>
        /// <param name="Id">私钥Id</param>
        /// <returns></returns>
        public async Task<RequestBack> AccessTokenDeleteAsync(int userId, int Id)
        {
            // 获取数据
            var accessToken = await _giteeapiContext.AccessTokens.FirstOrDefaultAsync(x =>
                Id == x.Id && x.UserId == userId);

            // 数据是否存在
            if (accessToken is null)
            {
                return new(0, "未找到数据");
            }

            // 修改
            accessToken.Delete = 1;

            // 保存
            if (await _giteeapiContext.SaveChangesAsync() > 0)
            {
                return new(1, "删除成功");
            }

            return new(0, "删除失败");
        }
    }
}