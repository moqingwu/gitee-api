﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace GiteeApi.Utils
{
    public class MD5Util
    {
        /// <summary>
        /// MD5 加密
        /// </summary>
        /// <param name="str">字符串</param>
        /// <returns>MD5</returns>
        public static string Get(string str)
        {
            var md5 = new MD5CryptoServiceProvider();
            byte[] buffer = md5.ComputeHash(Encoding.Default.GetBytes(str));
            return BitConverter.ToString(buffer).Replace("-", "");
        }
    }
}