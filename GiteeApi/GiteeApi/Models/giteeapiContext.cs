﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace GiteeApi.Models
{
    public partial class giteeapiContext : DbContext
    {
        public giteeapiContext()
        {
        }

        public giteeapiContext(DbContextOptions<giteeapiContext> options)
            : base(options)
        {
        }

        public virtual DbSet<AccessToken> AccessTokens { get; set; }
        public virtual DbSet<UserInfo> UserInfos { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseMySql("server=49.234.41.26;userid=GiteeApi;pwd=zpBGyxj3mA833aia;port=3306;database=giteeapi;sslmode=none", Microsoft.EntityFrameworkCore.ServerVersion.Parse("5.7.30-mysql"));
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasCharSet("utf8")
                .UseCollation("utf8_general_ci");

            modelBuilder.Entity<AccessToken>(entity =>
            {
                entity.ToTable("AccessToken");

                entity.Property(e => e.Id)
                    .HasColumnType("int(11)")
                    .HasComment("私钥Id");

                entity.Property(e => e.AccessToken1)
                    .IsRequired()
                    .HasMaxLength(255)
                    .HasColumnName("accessToken")
                    .HasComment("私钥");

                entity.Property(e => e.Alias)
                    .IsRequired()
                    .HasMaxLength(255)
                    .HasColumnName("alias")
                    .HasComment("别名");

                entity.Property(e => e.Delete)
                    .HasColumnType("tinyint(255)")
                    .HasComment("是否删除");

                entity.Property(e => e.Time)
                    .HasColumnType("datetime")
                    .HasComment("创建时间");

                entity.Property(e => e.UserId)
                    .HasColumnType("int(11)")
                    .HasComment("用户Id");
            });

            modelBuilder.Entity<UserInfo>(entity =>
            {
                entity.ToTable("UserInfo");

                entity.Property(e => e.Id)
                    .HasColumnType("int(11)")
                    .HasComment("用户Id");

                entity.Property(e => e.Delete)
                    .HasColumnType("tinyint(255)")
                    .HasComment("是否删除");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasComment("用户邮箱");

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasMaxLength(32)
                    .HasComment("用户密码");

                entity.Property(e => e.Time)
                    .HasColumnType("datetime")
                    .HasComment("创建时间");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
