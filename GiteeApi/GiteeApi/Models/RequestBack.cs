﻿using System.Collections.Generic;

namespace GiteeApi.Models
{
    /// <summary>
    /// 响应体
    /// </summary>
    public class RequestBack
    {
        /// <summary>
        /// 状态码
        /// </summary>
        public int code { get; set; }

        /// <summary>
        /// 信息
        /// </summary>
        public string message { get; set; }

        /// <summary>
        /// 数据
        /// </summary>
        public object data { get; set; }

        public RequestBack(int code, string message, object data = null)
        {
            this.code = code;
            this.message = message;
            this.data = data;
        }
    }
}
