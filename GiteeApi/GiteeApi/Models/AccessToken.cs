﻿using System;
using System.Collections.Generic;

#nullable disable

namespace GiteeApi.Models
{
    public partial class AccessToken
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string AccessToken1 { get; set; }
        public string Alias { get; set; }
        public DateTime Time { get; set; }
        public sbyte Delete { get; set; }
    }
}
