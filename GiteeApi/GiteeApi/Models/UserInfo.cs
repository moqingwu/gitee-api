﻿using System;
using System.Collections.Generic;

#nullable disable

namespace GiteeApi.Models
{
    public partial class UserInfo
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public DateTime Time { get; set; }
        public sbyte Delete { get; set; }
    }
}
