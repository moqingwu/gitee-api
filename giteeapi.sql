--
-- Table structure for table `AccessToken`
--

DROP TABLE IF EXISTS `AccessToken`;
CREATE TABLE `AccessToken` (
  `Id` int(11) NOT NULL AUTO_INCREMENT COMMENT '私钥Id',
  `UserId` int(11) NOT NULL COMMENT '用户Id',
  `accessToken` varchar(255) NOT NULL COMMENT '私钥',
  `alias` varchar(255) NOT NULL COMMENT '别名',
  `Time` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  `Delete` tinyint(255) NOT NULL DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Table structure for table `UserInfo`
--

DROP TABLE IF EXISTS `UserInfo`;
CREATE TABLE `UserInfo` (
  `Id` int(11) NOT NULL AUTO_INCREMENT COMMENT '用户Id',
  `Email` varchar(50) NOT NULL COMMENT '用户邮箱',
  `Password` varchar(32) NOT NULL COMMENT '用户密码',
  `Time` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  `Delete` tinyint(255) NOT NULL DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=10015 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
