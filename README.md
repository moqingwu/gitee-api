# Gitee AccessToken 管理平台

一个用于保存码云私钥，给我自己项目使用的玩意

用于前端方便的请求码云的全部 api



## 起因

目前我的两个小作品都利用了 [Gitee API 文档](https://gitee.com/api/v5/swagger#/) 来实现功能并且脱离数据库，但是他们 **都需要一个单独的后端** 来存储自己的私人令牌对码云的 api 进行请求交互

这样就会导致两个项目还需要部署两个后端，所以才有了这个项目，写一个管理平台，并且其他人使用我的开源项目时也可以跳过后端部署这个步骤，更加方便

本项目原理只是存储对应账号的私钥，通过不同用户的 id 与想要请求的 api 地址，代理请求码云 api 然后取得结果返回



## 如何部署

> Mysql

自行创建，然后跑 `giteeapi.sql` 脚本即可



> Redis

自行创建，项目默认访问本地地址

如需修改则在 `后端根目录/Services/RedisCacheHelper.cs` 中修改

```c#
// 设置地址
private static readonly string ConnectionWriteString = "127.0.0.1:6379";
```



> 后端（GiteeApi）

**请确保熟悉 .NET 项目部署**

修改 `appsettings.json` 中的配置，完善 JWT、Mysql 与 邮箱的配置

```json
{
    // JWT 加密
    "Authentication": {
        // 密钥 大于16位 随意填写
        "SecretKey": "",
        // 发布者
        "issuer": "N0ts",
        // 发布对象
        "audience": "N0ts"
    },
    // mysql 连接
    "MysqlConnection": {
        // 服务器地址
        "server": "",
        // 用户名
        "userid": "",
        // 密码
        "pwd": "",
        // 端口号
        "port": "3306",
        // 数据库名
        "database": ""
    },
    // 邮箱配置
    "Mail": {
        // 邮箱
        "UserName": "",
        // 私钥
        "Password": ""
    }
}

```



自行打包部署即可

部署成功后可访问 `后端地址/swagger` 来确认是否已成功部署

这是我目前部署成功后的 Swagger 界面：[Swagger UI (n0ts.cn)](https://giteeapi.n0ts.cn/swagger/index.html)

我部署在 Centos 中，使用宝塔的工具 **Supervisors管理器** 进行部署，如果部署在 windows 打包也会不一样，自行摸索



> 前端（Web）

修改 `前端根目录/js/api.js)` 文件

BaseURL 改成自己后端地址即可，url 结尾记得加上 `/`

```js
let BaseURL = "http://127.0.0.1:5000/";
```



访问前端进行测试即可

这是我部署成功的前端样子：[Gitee AccessToken 管理平台 (n0ts.cn)](https://gitee.n0ts.cn/)



## 如何请求码云 API

首先当然是要去前端注册一个账号，并且配置好自己的私钥，记一下自己的用户 Id



## GET

现在用 [仓库的所有 Issues](https://gitee.com/api/v5/swagger#/getV5ReposOwnerRepoIssues) 这个 api 举例

| URL                                                  | Method |
| ---------------------------------------------------- | ------ |
| https://gitee.com/api/v5/repos/{owner}/{repo}/issues | GET    |



先随便填写它必填的信息，然后点击测试，复制除去主域名的全部内容

```shell
# 原本请求地址
https://gitee.com/api/v5/repos/n0ts/jichou/issues?access_token=test&state=open&sort=created&direction=desc&page=1&per_page=20

# 除去主域名
api/v5/repos/n0ts/jichou/issues?access_token=test&state=open&sort=created&direction=desc&page=1&per_page=20
```



然后只需要这样请求即可拿到数据，请求体中的 access_token 数据改成字符串 `{0}`

```shell
# access_token={0}
api/v5/repos/n0ts/jichou/issues?access_token={0}&state=open&sort=created&direction=desc&page=1&per_page=20
```

| URL              | Method | Params           |
| ---------------- | ------ | ---------------- |
| 后端地址/用户 Id | Get    | path: "请求地址" |



Axios 请求演示

```js
axios
    .get("后端地址/用户 Id", {
    params: {
        path: `api/v5/repos/n0ts/jichou/issues?access_token={0}&sort=created&direction=desc&page=1&per_page=20`
    }
})
```



## POST / PUT / DELETE

用 [创建 Issue](https://gitee.com/api/v5/swagger#/postV5ReposOwnerIssues) 这个 api 举例

一样的复制除去主域名后的内容，请求地址一样是 `后端地址/用户 Id`

请求体则需要

```json
{
    path: "请求地址",
    password: "前端注册的密码",
    data: { ... } // 请求体
}
```



Axios 请求演示

axios 请求类型也需要对应接口需要的类型，这里是 post 请求

```js
axios
    .post("后端地址/用户 Id", {
    path: `api/v5/repos/n0ts/issues`,
    password: "123456",
    data: {
        // 请求体
        ...
    }
})
```



## 使用本项目的小作品

下面的一些小作品使用了本项目来请求码云 API

[heng: 利用码云Issues作为数据库的记仇小本本，独一无二的简约设计，支持富文本 (gitee.com)](https://gitee.com/n0ts/heng)

[EazyGiteeNote: 做一个简约实用，基于Gitee的学习笔记吧！](https://gitee.com/n0ts/eazy-gitee-note)

[IceCreamSearch: 一个极致简约的导航页。快捷搜索、一言、天气展示、自定义配置等等... (gitee.com)](https://gitee.com/n0ts/IceCreamSearch)
