/*
 * @Author: N0ts
 * @Date: 2021-10-20 11:09:26
 * @LastEditTime: 2021-10-29 09:31:39
 * @Description: 侧边栏
 * @FilePath: /Web/js/hooks/bar.js
 * @Mail：mail@n0ts.cn
 */

const { reactive, toRefs } = Vue;

export default function bar() {
    let data = reactive({
        barData: ["主面板", "Access Token", "修改密码"], // 侧边菜单数据
        barIndex: 0, // 侧边菜单当前选项
    });

    /**
     * 切换菜单
     * @param {*} i 索引
     */
    function changeBarIndex(i) {
        data.barIndex = i;
    }

    return {
        ...toRefs(data),
        changeBarIndex
    }
}