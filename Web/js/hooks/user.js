/*
 * @Author: N0ts
 * @Date: 2021-10-20 10:38:14
 * @LastEditTime: 2021-10-29 10:31:25
 * @Description: 用户操作
 * @FilePath: /Web/js/hooks/user.js
 * @Mail：mail@n0ts.cn
 */

const { reactive, toRefs } = Vue;

import $api from "../api.js";
import notifyJs from "./notify.js";
import access from "./access.js";

export default function UserHooks() {
    let data = reactive({
        loading: false, // 登陆注册加载屏障
        loginState: false, // 登陆状态
        userInfo: null, // 用户信息
        regPageState: false, // 登陆页是否展示
        email: "", // 登陆邮箱
        password: "", // 登陆密码
        regMail: "", // 注册邮箱
        regPassword: "", // 注册密码
        regPassword2: "", // 注册确认密码
        regCode: "", // 注册验证码
        mailSeconds: 60, // 发送验证码倒计时
        notMail: false, // 禁止邮件发送
        changePwdStr: "", // 修改密码
    });

    /**
     * 用户信息获取
     */
    data.userInfo = window.localStorage.getItem("userInfo");
    if (data.userInfo) {
        data.loginState = true;
        data.userInfo = JSON.parse(data.userInfo);
        // 加载数据
        loadData();
    }

    /**
     * 登陆页切换
     */
    function changeLogin() {
        data.regPageState = !data.regPageState;
    }

    /**
     * 登陆
     */
    function login() {
        data.loading = true;

        // 数据验证
        if (!data.email) {
            return notify("邮箱未填写", "warning");
        }
        if (!data.password) {
            return notify("密码未填写", "warning");
        }

        // 发送请求
        axios
            .post($api.Login, {
                email: data.email,
                password: data.password
            })
            .then((res) => {
                data.loading = false;

                // 是否请求成功
                if (!res || res.code == 0) {
                    return notify(res.message, "error");
                }

                // 登陆成功
                data.loginState = true;
                notify(res.message, "success");

                // 存储数据
                data.userInfo = res.data;
                window.localStorage.setItem("userInfo", JSON.stringify(data.userInfo));

                // 加载数据
                loadData();
            });
    }

    /**
     * 注册
     */
    function register() {
        data.loading = true;

        // 数据验证
        if (!data.regMail) {
            return notify("邮箱未填写", "warning");
        }
        if (!data.regPassword) {
            return notify("密码未填写", "warning");
        }
        if (!data.regPassword2) {
            return notify("确认密码未填写", "warning");
        }
        if (!data.regCode) {
            return notify("验证码未填写", "warning");
        }
        if (data.regPassword != data.regPassword2) {
            return notify("两次密码不一致", "warning");
        }

        // 注册
        axios
            .post($api.Register, {
                mailCode: data.regCode,
                email: data.regMail,
                password: data.regPassword,
                confirmPassword: data.regPassword2
            })
            .then((res) => {
                data.loading = false;

                // 是否请求成功
                if (!res || res.code == 0) {
                    return notify(res.message, "error");
                }

                notify(res.message, "success");

                data.regMail = null;
                data.regPassword = null;
                data.regPassword2 = null;
                data.regCode = null;
                changeLogin();
            });
    }

    /**
     * 发送注册邮件验证码
     */
    function goTimeOut() {
        data.loading = true;

        if (!data.regMail) {
            return notify("邮箱未填写", "warning");
        }

        // 发送邮件
        axios.post($api.GetRegisterMailCode, {
            mail: data.regMail
        }).then((res) => {
            data.loading = false;

            // 是否请求成功
            if (!res || res.code == 0) {
                return notify(res.message, "error");
            }

            notify(res.message, "success");

            // 开启计时
            data.notMail = true;
            let timeOut = setInterval(() => {
                if (data.mailSeconds-- == 0) {
                    data.mailSeconds = 60;
                    data.notMail = false;
                    clearInterval(timeOut);
                }
            }, 1000);
        });
    }

    /**
     * 修改密码
     */
    function ChangePwd() {
        data.loading = true;

        // 数据验证
        if(!data.changePwdStr) {
            return notify("密码不能为空！", "warning");
        }

        // 修改密码
        axios.put($api.ChangePwd, {
            password: data.changePwdStr
        }).then((res) => {
            data.loading = false;

            // 是否请求成功
            if (!res || res.code == 0) {
                return notify(res.message, "error");
            }

            notify(res.message, "success");
        });
    }

    /**
     * 加载数据
     */
    function loadData() {
        access().getAccess();
    }

    /**
     * 提示框
     * @param {*} message 消息
     * @param {*} type 类型
     */
    function notify(message, type) {
        data.loading = false;
        notifyJs(message, type);
    }

    return {
        ...toRefs(data),
        login,
        changeLogin,
        register,
        goTimeOut,
        ChangePwd
    };
}
