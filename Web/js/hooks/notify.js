/*
 * @Author: N0ts
 * @Date: 2021-10-20 13:17:02
 * @LastEditTime: 2021-10-20 15:50:20
 * @Description: 消息提示
 * @FilePath: /Web/js/hooks/notify.js
 * @Mail：mail@n0ts.cn
 */

const { ElNotification } = ElementPlus;

export default function notify(message, type) {
    ElNotification({
        message,
        type,
        showClose: true
    });
}
