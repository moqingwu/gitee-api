/*
 * @Author: N0ts
 * @Date: 2021-10-19 20:58:49
 * @LastEditTime: 2021-10-29 09:51:21
 * @Description: API 列表
 * @FilePath: /Web/js/api.js
 * @Mail：mail@n0ts.cn
 */

let BaseURL = "http://127.0.0.1:5000/";

export default {
    // 登陆
    Login: BaseURL + "api/User/Login",
    // 注册获取邮箱验证码
    GetRegisterMailCode: BaseURL + "api/User/GetRegisterMailCode",
    // 注册
    Register: BaseURL + "api/User/Register",
    // 找回密码
    ChangePwd: BaseURL + "api/User/ChangePwd",
    // 添加私钥
    AccessTokenAdd: BaseURL + "api/AccessToken/Add",
    // 修改私钥
    AccessTokenUpdate: BaseURL + "api/AccessToken/Update",
    // 获取私钥
    AccessTokenGet: BaseURL + "api/AccessToken/Get",
    // 删除私钥
    AccessTokenDelete: BaseURL + "api/AccessToken/Delete",
}